'use strict';

$(function(){
  $('#reg-form').toggleClass('visible-xs visible-sm hidden');
  $('#join-btn').addClass('visible-md visible-lg');
});

  //Show form on #joinBtn click
$('#join-btn').click(function() {
  $('#join-btn').removeClass('visible-md visible-lg');
  // Move logo a little to the top
  $('.jumbotron').animate({ 'padding-top': '-50px'}, 150);
  // Draw dark translucent box in the background and show form
  $('#reg-form').fadeIn(200,function(){
    $(this).addClass('visible-md visible-lg');
  });
});
